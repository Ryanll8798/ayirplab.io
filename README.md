# ayirp.gitlab.io

Priya's personal website: <https://ayirp.gitlab.io>

## Serving this site locally

1.  Install [Ruby](https://www.ruby-lang.org/en/).
2.  Clone [this repository](https://gitlab.com/ayirp/ayirp.gitlab.io).
3.  Run the following to navigate to the website folder's directory and install all requirements:

    ```sh
    cd ayirp.gitlab.io
    gem install bundler
    bundle install
    ```

4.  Serve the site:

    ```sh
    bundle exec jekyll serve
    ```

5.  View the site at `http://localhost:4000`.

## License

Copyright (c) 2020-2021 Priya Streethran.
This site's source code is licensed under the terms of the [MIT License](https://opensource.org/licenses/MIT).

---

This website is powered by [Minimal Mistakes](https://mademistakes.com/work/minimal-mistakes-jekyll-theme/).
Copyright (c) 2013-2020 Michael Rose and contributors.
Minimal Mistakes is distributed under the terms of the MIT License.

Minimal Mistakes is a [Jekyll](https://jekyllrb.com/) theme.
Copyright (c) 2008-present Tom Preston-Werner and Jekyll contributors.
Jekyll is distributed under the terms of the MIT License.

This site is hosted using [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

---

Minimal Mistakes incorporates icons from [The Noun Project](https://thenounproject.com/).
Created by Garrett Knoll, Arthur Shlain, and tracy tam.
Icons are distributed under a [Creative Commons Attribution 3.0 United States (CC-BY-3.0-US) license](https://creativecommons.org/licenses/by/3.0/us/).

Minimal Mistakes incorporates [Font Awesome](https://fontawesome.com/).
Copyright (c) 2017 Dave Gandy.
Font Awesome is distributed under the terms of the [SIL Open Font License 1.1 (OFL-1.1)](https://opensource.org/licenses/OFL-1.1) and MIT License.

Minimal Mistakes incorporates public domain photographs from [Unsplash](https://unsplash.com).
Photos are distributed under the terms of the [Unsplash License](https://unsplash.com/license).

Minimal Mistakes incorporates [Susy](https://www.oddbird.net/susy/).
Copyright (c) 2017 Miriam Eric Suzanne.
Susy is distributed under the terms of the [BSD 3-clause "New" or "Revised" License (BSD-3-Clause)](https://opensource.org/licenses/BSD-3-Clause).

Minimal Mistakes incorporates [Breakpoint](http://breakpoint-sass.com/).
Breakpoint is distributed under the terms of the MIT License or [GNU General Public License, version 2 (GPL-2.0)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html).

Minimal Mistakes incorporates [FitVids.js](https://github.com/davatron5000/FitVids.js/).
Copyright (c) 2013 Dave Rubert and Chris Coyier.
FitVids is distributed under the terms of the [WTFPL License](http://www.wtfpl.net/).

Minimal Mistakes incorporates [Magnific Popup](https://dimsemenov.com/plugins/magnific-popup/).
Copyright (c) 2014-2016 [Dmitry Semenov](https://dimsemenov.com).
Magnific Popup is distributed under the terms of the MIT License.

Minimal Mistakes incorporates [Smooth Scroll](https://github.com/cferdinandi/smooth-scroll).
Copyright (c) 2019 Chris Ferdinandi.
Smooth Scroll is distributed under the terms of the MIT License.

Minimal Mistakes incorporates [Gumshoe](https://github.com/cferdinandi/gumshoe).
Copyright (c) 2019 Chris Ferdinandi.
Gumshoe is distributed under the terms of the MIT License.

Minimal Mistakes incorporates [jQuery throttle / debounce](http://benalman.com/projects/jquery-throttle-debounce-plugin/).
Copyright (c) 2010 "Cowboy" Ben Alman.
jQuery throttle / debounce is distributed under the terms of the MIT License.

Minimal Mistakes incorporates [GreedyNav.js](https://github.com/lukejacksonn/GreedyNav).
Copyright (c) 2015 Luke Jackson.
GreedyNav.js is distributed under the terms of the MIT License.

Minimal Mistakes incorporates [Jekyll Group-By-Array](https://github.com/mushishi78/jekyll-group-by-array).
Copyright (c) 2015 Max White (mushishi78 at gmail dot com).
Jekyll Group-By-Array is distributed under the terms of the MIT License.

Minimal Mistakes incorporates [@allejo's Pure Liquid Jekyll Table of Contents](https://allejo.io/blog/a-jekyll-toc-in-liquid-only/).
Copyright (c) 2017 Vladimir Jimenez.
Pure Liquid Jekyll Table of Contents is distributed under the terms of the MIT License.

Minimal Mistakes incorporates [Lunr](https://lunrjs.com).
Copyright (c) 2018 Oliver Nightingale.
Lunr is distributed under the terms of the MIT License.

---

This website incorporates comments powered by [Staticman](https://staticman.net/) by [Eduardo Bouças](https://eduardoboucas.com/), [Heroku](https://www.heroku.com/), and [GitLab](https://about.gitlab.com/).
Staticman is distributed under the terms of the MIT License.

This site incorporates `emoji_u1f338` from [Noto Emoji, v2017-05-18-cook-color-fix](https://github.com/googlefonts/noto-emoji/tree/v2017-05-18-cook-color-fix) as the icon.
Copyright (c) 2017 Google Inc.
Images are distributed under the terms of the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0).

This website incorporates the [Homemade Apple](https://fonts.google.com/specimen/Homemade+Apple) font by Font Diner.
This font is distributed under the terms of the Apache License, Version 2.0.

## Privacy

All comments posted will be available publicly on [GitLab](https://gitlab.com/ayirp/ayirp.gitlab.io/-/tree/master/_data/comments).
An email address is required to post comments, but it will not be published.
Email addresses are encrypted using [MD5](https://en.wikipedia.org/wiki/MD5).

<!-- Due to an increase in spam comments, this site uses Google's [reCAPTCHA](https://developers.google.com/recaptcha/) v2.
See Google's [Privacy Policy](https://policies.google.com/privacy?hl=en) and [Terms of Service](https://policies.google.com/terms?hl=en). -->
